let cut_empties lst =
  List.filter (fun elem -> match elem with "" -> false | _ -> true) lst

(*-------------------------------------------------------------------------------*)
let str_lst_to_file filename data_lst =
  let rec to_string lst =
    match lst with
    | head :: medium :: tail -> head ^ " " ^ to_string (medium :: tail)
    | [head] -> head
    | [] -> "" in
  let data = to_string data_lst in
  let oc = open_out filename in
  (* create or truncate file, return channel *)
  Printf.fprintf oc "%s" data;
  (* write something *)
  close_out oc

(* flush and close the channel *)

let str_lst_from_file filename =
  let read_whole_file name =
    let ch = try open_in name with _ -> failwith ("BAD FILE(" ^ name ^ ")") in
    let s =
      try really_input_string ch (in_channel_length ch)
      with _ -> close_in_noerr ch; failwith "BAD READ" in
    close_in ch; s in
  let splitter str =
    let lst = String.split_on_char '\n' str in
    let sec_lst =
      List.concat (List.map (fun elem -> String.split_on_char ' ' elem) lst)
    in
    cut_empties sec_lst in
  let my_str = read_whole_file filename in
  splitter my_str
